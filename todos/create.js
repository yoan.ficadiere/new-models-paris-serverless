'use strict';

const uuid = require('uuid');
const dynamodb = require('./dynamodb');

module.exports.create = (event, context, callback) => {
  const timestamp = new Date().getTime();
  const params = {
    TableName: process.env.DYNAMODB_TABLE,
    Item: {
      sex: "women",
      name: "Maeva",
      surname: "lanis",
      birthdate: "1995-10-09",
      phone: "+33689283745",
      email: "maeva@gmail.com",
      city: "Paris",
      password: "pdzoe9e",
      height: 174,
      hairColor: "blonds",
      eyesColor: "black",
      extraType: ["underwear", "bikini", "photoShoot"]
    }
  };

  dynamodb.put(params, (error) => {
    if (error) {
      console.error(error);
      callback(null, {
        statusCode: error.statusCode || 501,
        body: JSON.stringify({
          message: error.message
        }),
      });
      return;
    }

    // create a response
    const response = {
      statusCode: 200,
      body: JSON.stringify(params.Item),
    };
    callback(null, response);
  });
};
