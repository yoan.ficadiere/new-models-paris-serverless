const dynamodb = require('../utils/dynamodb')
const { findRegion } = require('../utils/region/findRegion')
/**
 * The User Repository for admin
 */
 class UsersRepository {
  get _baseParams() {
    return {
      TableName: process.env.DYNAMODB_TABLE
    }
  }

  /**
  * @function getReviewedUsersStatus
  * @description récupére la liste des utilisateur qui n'on pas encore
  * examiné ou non
  * @param {boolean} isReviewed value
  */
  async getReviewedUsersStatus(isReviewed) {
    const params = this._createParamObject({
      ExpressionAttributeValues: {
        ':isReviewed': isReviewed
      },
      FilterExpression: 'isReviewed = :isReviewed',
      ProjectionExpression: 'firstname, lastname, birthdate, id, photos, createdAt, isReviewed'
    })
    let result = await dynamodb.scan(params).promise();
    return result.Items
  }

  /**
  * @function getPublicUserStatus
  * @description récupére la liste des utilisateur qui sont public ou non
  * @param {boolean} isPublic value
  */
  async getPublicUserStatus(isPublic) {
    const params = this._createParamObject({
      ExpressionAttributeValues: {
        ':isPublic': isPublic,
        ':isValidate': true
      },
      FilterExpression: 'isPublic = :isPublic AND isValidate = :isValidate',
      ProjectionExpression: 'firstname, lastname, birthdate, id, photos, createdAt, isReviewed, isValidate'
    })
    let result = await dynamodb.scan(params).promise();
    return result.Items
  }

  /**
  * @function getUser
  * @description récupére toute les informations d'un utilisateur par
  * rapport à son id
  * @param {string} id - Id de l'utilisateur ( partition key )
  */
  async getUser(id) {
     const params = this._createParamObject({
      KeyConditionExpression: 'id = :id',
      ExpressionAttributeValues: {
          ':id': id,
       }
     })
   let result = await dynamodb.query(params).promise()
   return result.Items[0]
 }



  /**
  * @function reviewUser
  * @description Function qui permet de valider ou non un utilisateur si
  * l'utilisateur est valider il est automatiquement rendu public et la premiere
  * photo de son profile est considéré comme valider
  * @param {string} id - Id de l'utilisateur ( partition key )
  * @param {number} createdAt - Date de créationd de l'utilisateur ( sort key )
  * @param {boolean} isValidate true si l'utilsateur doit être valider
  * false si l'utilisateur ne doit pas être validé
  */
  async reviewUser(id, createdAt, isValidate) {
    const params = this._createParamObject({
      Key: {
        id,
        createdAt
      },
      ConditionExpression: 'attribute_exists(id)',
      UpdateExpression: 'set isValidate = :isValidate, isReviewed = :reviewed, isPublic = :isPublic',
      ExpressionAttributeValues: {
       ':isValidate': isValidate,
       ':isPublic': isValidate ? true : false,
       ':reviewed': true
      },
      ReturnValues: 'UPDATED_NEW'
    })
    let result = await dynamodb.update(params).promise()
    return result.Attributes
  }

  /**
  * @function publicUser
  * @description Function qui permet de rendre publique ou non un utilisateur
  * @param {string} id - Id de l'utilisateur ( partition key )
  * @param {number} createdAt - Date de créationd de l'utilisateur ( sort key )
  * @param {boolean} isPublic true si l'utilsateur doit être public
  * false si l'utilisateur ne doit pas être public
  */
  async publicUser(id, createdAt, isPublic) {
    const params = this._createParamObject({
      Key: {
        id,
        createdAt
      },
      ConditionExpression: 'attribute_exists(id) and isValidate = :bool',
      UpdateExpression: 'set isPublic = :public',
      ExpressionAttributeValues: {
       ':public': isPublic,
       ':bool': true
      },
      ReturnValues: 'UPDATED_NEW'
    })
    let result = await dynamodb.update(params).promise()
    return result.Attributes
  }

  /**
  * @function getValidateUsers
  * @description Function qui permet de récupéré la liste des utilisateurs validé
  * @return {array{}} Tableau contenant les utilisateurs validé
  */
  async getValidateUsers () {
    const params = this._createParamObject({
      ExpressionAttributeValues: {
        ':value': true,
        ':reviewed': true
      },
      FilterExpression: 'isValidate = :value AND isReviewed = :reviewed',
      ProjectionExpression: 'firstname, lastname, birthdate, id, photos'
    })
    let result = await dynamodb.scan(params).promise();
    return result.Items
  }

  /**
  * @function getRejectUsers
  * @description Function qui permet de récupéré la liste des utilisateurs rejetté
  * @return {array{}} Tableau contenant les utilisateurs rejeté
  */
  async getRejectUsers () {
    const params = this._createParamObject({
      ExpressionAttributeValues: {
        ':value': false,
        ':reviewed': true
      },
      FilterExpression: 'isValidate = :value AND isReviewed = :reviewed',
      ProjectionExpression: 'firstname, lastname, birthdate, id, photos'
    })
    let result = await dynamodb.scan(params).promise();
    return result.Items
  }

  /**
  * @function updatePhotos
  * @description Function qui permet de mettre à jour les photos de l'utilisateur
  * @param {string} id - Id de l'utilisateur ( partition key )
  * @param {number} createdAt - Date de création de l'utilisateur ( sort key )
  * @params {array{}} photos - photos de l'utilisateur
  */
  async updatePhotos (id, createdAt, photos) {
    const params = this._createParamObject({
      Key: {
        id,
        createdAt
      },
      ConditionExpression: 'attribute_exists(id)',
      UpdateExpression: 'set photos = :photos',
      ExpressionAttributeValues: {
       ':photos': photos
      },
      ReturnValues: 'UPDATED_NEW'
    })
    let result = await dynamodb.update(params).promise()
    return result.Attributes
  }

  /**
  * @function updateUser
  * @description Function qui permet de mettre à jour les informations sur
  * un utilisateur
  * @param {string} id - Id de l'utilisateur ( partition key )
  * @param {number} createdAt - Date de création de l'utilisateur ( sort key )
  * @params {object} informations - informations mise à jour
  */
  async updateUser (id, createdAt, informations) {
    const {
      sex,
      firstname,
      lastname,
      instagram,
      phone,
      city,
      postCode,
      height,
      shoeSize,
      hairColor,
      eyesColor,
      clothes,
      extraType,
      origin
    } = informations
    const region  = findRegion(postCode);
    const params = this._createParamObject({
      Key: {
        id,
        createdAt
      },
      ConditionExpression: 'attribute_exists(id)',
      UpdateExpression: `set
        sex = :sex,
        firstname = :firstname,
        lastname = :lastname,
        instagram = :instagram,
        origin = :origin,
        phone = :phone,
        city = :city,
        postCode = :postCode,
        regionName = :regionName,
        height = :height,
        shoeSize = :shoeSize,
        hairColor = :hairColor,
        eyesColor = :eyesColor,
        clothes = :clothes,
        extraType = :extraType`,
      ExpressionAttributeValues: {
       ':sex': sex,
       ':firstname': firstname,
       ':lastname': lastname,
       ':instagram': instagram ? instagram : 'unknown',
       ':origin': origin,
       ':phone': phone,
       ':city': city,
       ':postCode': postCode,
       ':regionName': region ? region.regionName : 'unknown',
       ':height': height,
       ':shoeSize': shoeSize,
       ':hairColor': hairColor,
       ':eyesColor': eyesColor,
       ':extraType': extraType,
       ':clothes': clothes,
      },
      ReturnValues: 'UPDATED_NEW'
    })
    let result = await dynamodb.update(params).promise()
    return result.Attributes
  }

  _createParamObject(additionalArgs = {}) {
    return {
      ...this._baseParams,
      ...additionalArgs
    }
  }
}

exports.UsersRepository = UsersRepository;
