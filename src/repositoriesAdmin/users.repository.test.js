const { UsersRepository } = require('./users.repository.js')

const usersRepository = new UsersRepository()

describe('Testing user repository', () => {
	test('should return only unreviewed user', async () => {
		let users = await usersRepository.getReviewedUsersStatus(false)
		expect(users.every((user) => user.isReviewed === false)).toBe(true)
	})

	test('should return only rejected user', async () => {
		let result = await usersRepository.getRejectUsers()
		expect(result[0].id).toBe('16a7d272-2a19-4876-a4e0-9920909c4026')
	})

	test('should return only validate user', async () => {
		let result = await usersRepository.getValidateUsers()
		expect(result[0].id).toBe('162ec951-f471-4351-b63d-4d6ef4bf85ed')
	})

	test('return all propertie of user with id 16a7d272-2a19-4876-a4e0-9920909c4026', async () => {
		const user =  {
			'birthdate': '10-10-1993',
			'city': 'Paris',
			'createdAt': 1548370864094,
			'email': 'yoyo@gmail.com',
			'extraType': [
				'musicVideo',
				'vixen'
			],
			'eyesColor': 'black',
			'firstname': 'Marine',
			'hairColor': 'darkBlond',
			'height': 132,
			'id': '16a7d272-2a19-4876-a4e0-9920909c4026',
			'isPublic': false,
			'isReviewed': true,
			'isValidate': false,
			'lastname': 'Danis',
			'phone': '0682938473',
			'photos': [{
				'id': 'id',
				'key': 'key'
			}, {
				'id': 'id',
				'key': 'key'
			}],
			'postCode': '93200',
			'sex': 'men'
		}
		let result = await usersRepository.getUser('16a7d272-2a19-4876-a4e0-9920909c4026')
		expect(result).toEqual(user)
	})

	test('should review and validate user', async () => {
		let result = await usersRepository.reviewUser('16a7d272-2a19-4876-a4e0-9920909c4026', 1548370864094, true)
		expect(result).toEqual({ isValidate: true, isReviewed: true })
	})

	test('should review and not validate user', async () => {
		let result = await usersRepository.reviewUser('16a7d272-2a19-4876-a4e0-9920909c4026', 1548370864094, false)
		expect(result).toEqual({ isValidate: false, isReviewed: true })
	})

	test('should render user public', async () => {
		await usersRepository.reviewUser('16a7d272-2a19-4876-a4e0-9920909c4026', 1548370864094, true)
		let result = await usersRepository.publicUser('16a7d272-2a19-4876-a4e0-9920909c4026', 1548370864094, true)
		expect(result).toEqual({ isPublic: true })
	})

	test('should render user public', async () => {
		await usersRepository.reviewUser('16a7d272-2a19-4876-a4e0-9920909c4026', 1548370864094, true)
		let result = await usersRepository.publicUser('16a7d272-2a19-4876-a4e0-9920909c4026', 1548370864094, false)
		expect(result).toEqual({ isPublic: false })
	})
	test('should update photo', async () => {
		let newPhotosArray = [{key: 'the most beautiful weed tree picture', id: 'id of weed'}]
		let userBeforeChangePhotos = await usersRepository.getUser('16a7d272-2a19-4876-a4e0-9920909c4026')
		expect(userBeforeChangePhotos.photos).not.toEqual(newPhotosArray)
		await usersRepository.updatePhotos('16a7d272-2a19-4876-a4e0-9920909c4026', 1548370864094, newPhotosArray)
		let userAfterChangePhotos = await usersRepository.getUser('16a7d272-2a19-4876-a4e0-9920909c4026')
		expect(userAfterChangePhotos.photos).toEqual(newPhotosArray)
	})
})
