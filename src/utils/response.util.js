/**
 *
 * @descripion Creates a response with a given status code and a formatter
 * @param {number} statusCode The response status code
 * @param {(data: any)} data The response data
 */
module.exports.response = (statusCode, data = {}) => {
  return {
    'headers': {
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Credentials': true
    },
    statusCode: statusCode,
    body: JSON.stringify(data)
  }
};
