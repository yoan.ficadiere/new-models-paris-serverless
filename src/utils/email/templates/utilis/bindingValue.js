module.exports.extraTypeBinding = [
  {
    name: 'Court métrage',
    value: 'shortFilms'
  },
  {
    name: 'Clips vidéos',
    value: 'musicVideo'
  },
  {
    name: 'Shootings photos',
    value: 'photoShoot'
  },
  {
    name: 'Défilé de mode',
    value: 'fashionShow'
  }
]

module.exports.clothesBinding = [
  {
    name: 'Fashion',
    value: 'fashionMode'
  },
  {
    name: 'Fitness',
    value: 'fitness'
  },
  {
    name: 'Bikini',
    value: 'bikini'
  },
  {
    name: 'Lingerie',
    value: 'lingerie'
  },
  {
    name: 'Sous-vêtement',
    value: 'underwear'
  },
  {
    name: 'Vixen',
    value: 'vixen'
  },
  {
    name: 'Nu',
    value: 'nude'
  }
]
