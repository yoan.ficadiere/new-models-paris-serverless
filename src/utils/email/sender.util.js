const aws = require('aws-sdk');
const nodemailer = require('nodemailer');
const { getMailOption } = require('./getMailOption');

const ses = new aws.SES();

/**
* @function sender
* @description Permet d'envoyer des emails
* @parmas {object} emailInformations - Information pour ajouter dans le mail
* @parmas {string} templateName - Nom du template à utiliser pour envoyé les emails
*/
module.exports.sender = async (templateName, emailInformations) => {
    // create Nodemailer SES transporter
    const transporter = nodemailer.createTransport({
        SES: ses
    });
    // send email
    return transporter.sendMail(getMailOption(templateName, emailInformations))
};
