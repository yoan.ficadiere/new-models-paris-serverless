const { validateUserTemplate } = require('./templates/validateUser');
const { enquireRequestTemplate } = require('./templates/enquireRequest');
const { enquireRequestFeedBackTemplate } = require('./templates/enquireRequestFeedBack');
const { rejectUserAnyTemplate } = require('./templates/rejectUserAny');
const { rejectUserInvalidPhotosTemplate } = require('./templates/rejectUserInvalidPhotos');

let whiteListe = [
  'yoan.ficadiere@gmail.com',
  'kendy@newmodelsparis.com'
]

/**
* @function getMailOption
* @description Permet d'envoyer des emails
* @parmas {object} emailInformations - Information pour ajouter dans le mail
* @parmas {string} templateName - Nom du template à utiliser pour envoyé les emails
*/
module.exports.getMailOption = (templateName, emailInformations) => {
  if (process.env.STAGE === 'dev' && !whiteListe.includes(emailInformations.email)) {
    return {
      from: 'New Models Paris <casting@newmodelsparis.com>',
      subject: 'This mail is from stage dev ... 🛠️ - ⚠️ Utilsation d\'une adresse mail non white listé ⚠️',
      html: validateUserTemplate(emailInformations),
      to: 'yoan.ficadiere@gmail.com'
    };
  }
  switch (templateName) {
    case 'validateUser':
      return {
        from: 'New Models Paris <casting@newmodelsparis.com>',
        subject:  process.env.STAGE === 'dev' ? 'This mail is from stage dev ... 🛠️ - Bienvenue dans l’agence New Models Paris 🎉' : 'Bienvenue dans l’agence New Models Paris 🎉',
        html: validateUserTemplate(emailInformations),
        to: emailInformations.email
      };
    case 'enquireRequest':
    return {
      from: 'New Models Paris <enquire@newmodelsparis.com>',
      subject: process.env.STAGE === 'dev' ? 'This mail is from stage dev ... 🛠️ - Demande de booking 🎉' :'Demande de booking 🎉',
      html: enquireRequestTemplate(emailInformations),
      to: 'kendy@newmodelsparis.com, yoan.ficadiere@gmail.com',
      replyTo: emailInformations.enquire.email
    };
    case 'enquireRequestFeedBack':
    return {
      from: 'New Models Paris <enquire@newmodelsparis.com>',
      subject: process.env.STAGE === 'dev' ? 'This mail is from stage dev ... 🛠️ - Nous avons bien reçu votre demande' :'Nous avons bien reçu votre demande',
      html: enquireRequestFeedBackTemplate(emailInformations),
      to: emailInformations.enquire.email
    };
    case 'any':
    return {
      from: 'New Models Paris <casting@newmodelsparis.com>',
      subject: process.env.STAGE === 'dev' ? 'This mail is from stage dev ... 🛠️ - Votre candidature' : 'Votre candidature',
      html: rejectUserAnyTemplate(emailInformations),
      to: emailInformations.email
    };
    case 'invalidPhotos':
    return {
      from: 'New Models Paris <casting@newmodelsparis.com>',
      subject: process.env.STAGE === 'dev' ? 'This mail is from stage dev ... 🛠️ - Photos non conformes' :'Photos non conformes',
      html: rejectUserInvalidPhotosTemplate(emailInformations),
      to: emailInformations.email
    };
    default:
      console.log('no template name specify');
  }
}
