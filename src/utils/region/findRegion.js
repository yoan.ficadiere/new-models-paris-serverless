const { regions } = require('./regions');

/**
* @function findRegion
* @description Function qui permet de récupéréer une
* région par rapport à un code postale
* @param {string} postCode - postCode code postale ( partition key )
* @return {string} region associé au code postale
*/
module.exports.findRegion = (postCode) => regions.find((region) => region.departmentCode === postCode.slice(0, 2));
