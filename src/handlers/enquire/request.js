'use strict'
const { EnquireRepository } = require('../../repositories/enquire.repository.js');
const { response } = require('../../utils/response.util');

const enquireRepository = new EnquireRepository();

/**
* @function request
* @description Function permettant de transmettre une demande
* de booking par mail au staff
*/
module.exports.request = async (event) => {
	const data = JSON.parse(event.body)
	try {
		await enquireRepository.sendEnquireRequest(data);
		return response(200, {
			success: true
		})
	} catch (error) {
		return response(error.statusCode || 501, {message: error.message})
	}
}
