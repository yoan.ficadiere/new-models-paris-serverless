'use strict'

const { UsersRepository } = require('../../repositories/users.repository');
const { response } = require('../../utils/response.util');

const usersRepository = new UsersRepository();

/**
* @function getPublicUser
* @description function permettant de récupéré les informations
* d'un utilisateur public
*/

module.exports.getPublicUser = async (event) => {
	try {
		let user = await usersRepository.getPublicUser(event.pathParameters.id)
		if (!user) {
			return response(200, { message: `User width id " ${event.pathParameters.id} " not found or not public` })
		}
		return response(200, user)
	} catch (error) {
		return response(error.statusCode || 501, {message: error.message})
	}
}
