'use strict'

const { UploadPhotoRepository } = require('../../repositories/uploadPhoto.repository');
const { response } = require('../../utils/response.util');

const uploadPhotoRepository = new UploadPhotoRepository();

/**
* @function uploadPhoto
* @description function permettant d'uploader les photos de l'utilisateur vers S3
*/

module.exports.uploadPhoto = async (event) => {
  const data = JSON.parse(event.body)
	try {
    let photoUrl = await uploadPhotoRepository.uploadPhoto(data.base64, data.id)
    return response(200, {url: photoUrl})
	} catch (error) {
		return response(error.statusCode || 501, {message: error.message})
	}
}
