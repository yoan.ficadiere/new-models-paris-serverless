'use strict'

const { UsersRepository } = require('../../repositories/users.repository');
const { response } = require('../../utils/response.util');

const usersRepository = new UsersRepository();

/**
* @function getPublicUsers
* @description function permettant de récupéré tous les utilisateur public par sex
* @params {string} sex - Sex des utilisateurs à récupéré
*/

module.exports.getPublicUsers = async (event) => {
	const { sex } = event.queryStringParameters
	try {
		let result = await usersRepository.getPublicUsers(sex)
		return response(200, result)
	} catch (error) {
		return response(error.statusCode || 501, {message: error.message})
	}
}
