'use strict'
const { UsersRepository } = require('../../repositories/users.repository.js');
const { response } = require('../../utils/response.util');

const usersRepository = new UsersRepository();

/**
* @function create
* @description function permettant de créer de nouveau utilisateur
* avec les informations du formulaire become a model
*/
module.exports.create = async (event) => {
	const data = JSON.parse(event.body)
	try {
		await usersRepository.create(data);
		return response(200, {
			success: true
		})
	} catch (error) {
		return response(error.statusCode || 501, {message: error.message})
	}
}
