const { UsersRepository } = require('../repositoriesAdmin/users.repository');
const { sender } = require('../utils/email/sender.util');

const usersRepository = new UsersRepository();

/**
 * The Enquire Repository
 */
 class EnquireRepository {
  get _baseParams() {
    return {
      TableName: process.env.DYNAMODB_TABLE
    };
  }

  /**
  * @function sendEnquireRequest
  * @description Envoie un mail au staff avec la demande de renseignement faite par les utilisateurs
  * @param {object} data - Id des l'utilisateur pour lequel à étais fait une demande et information du formulaire
  */
  async sendEnquireRequest(data) {
    console.log(data);
    let users = await Promise.all(data.usersId.map(id => usersRepository.getUser(id)))
    let selectedModels = users.map((user) => ({
      photo: user.photos[0],
      firstname: user.firstname,
      publicProfileUrl: process.env.STAGE === 'dev' ? `https://dev.newmodelsparis.com/profile/${user.id}` : `https://www.newmodelsparis.com/profile/${user.id}`
    }))

    // Envoie de la demande de modèle au staff new models paris
    console.log('Envoie de l\'email au staff de new models paris');
    await sender('enquireRequest', {selectedModels, ...data })

    // Envoie de la confirmation de la demande de modele au demandeur
    console.log('Envoie de l\'email au demandeur');
    await sender('enquireRequestFeedBack', {selectedModels, ...data })
    console.log('Données des utilisateurs pour qui une demande de renseignement à étais faite');
    console.log({selectedModels, ...data });
    console.log('Information du formulaire');
    console.log(data.enquire);
    return {}
  }

  _createParamObject(additionalArgs = {}) {
    return {
      ...this._baseParams,
      ...additionalArgs
    }
  }
}

exports.EnquireRepository = EnquireRepository
