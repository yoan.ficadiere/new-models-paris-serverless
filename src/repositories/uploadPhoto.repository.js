const AWS = require('aws-sdk')
const s3 = new AWS.S3()

/**
 * The Upload Photo Repository
 */
 class UploadPhotoRepository {
 /**
  * Upload user photo
  * @param {string} base64 Photo en base 64 à uploader
  * @returns {Promise} url de la photo
  */
  async uploadPhoto(base64String, id) {
     let decodedImage = Buffer.from(base64String.replace(/^data:image\/\w+;base64,/, ''), 'base64')
     var filePath = id + '/' + new Date().getTime() + '.jpg'
     var params = {
       Body: decodedImage,
       Bucket: process.env.STAGE === 'dev' ? 'dev-upload-new-models-paris' : 'upload-new-models-paris',
       Key: filePath,
       ContentType: 'image/jpeg',
       ACL: 'public-read'
    }
    let result = await s3.upload(params).promise()
    console.log(result)
    return result.Location
  }
}

exports.UploadPhotoRepository = UploadPhotoRepository
