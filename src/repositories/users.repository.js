const dynamodb = require('../utils/dynamodb')
var moment = require('moment');
const { findRegion } = require('../utils/region/findRegion');


/**
 * The User Repository
 */
 class UsersRepository {
  get _baseParams() {
    return {
      TableName: process.env.DYNAMODB_TABLE
    };
  }

  /**
   * Add or replace a contact
   * @param {object} userInformations The userInformations
   * @returns {Promis} The user informations
   */
  async create(userInformations) {
    const timestamp = new Date().getTime()
    // Ajout de l'information photo non valider pour chaque photo de l'utilisateur
    let photos = userInformations.photos.map((photo, index) => (index === 0 ? {...photo, isValidate: true } : { ...photo, isValidate: false }))
    // Ajout de la région de l'utilisateur par rapport à sont code postale
    let region = findRegion(userInformations.postCode);

    const params = this._createParamObject({
      Item: {
        ...userInformations,
        photos,
        regionName: region ? region.regionName : 'unknown',
        createdAt: timestamp,
        updatedAt: timestamp,
        birthdateTimestamp: moment(userInformations.birthdate, 'MM-DD-YYYY').unix(),
        isReviewed: false,
        isValidate: false,
        isPublic: false
      }
    });
    await dynamodb.put(params).promise();
    return userInformations;
  }

  /**
  * @function getPublicUsers
  * @description récupére tous les utilisateurs public
  * @param {string} sex - Sex des utilisateurs à récupéré
  */
  async getPublicUsers(sex) {
    const params = this._createParamObject({
      ExpressionAttributeValues: {
        ':public': true,
        ':sex': sex
      },
      FilterExpression: 'isPublic = :public AND sex = :sex',
      ProjectionExpression: 'firstname, id, photos, createdAt'
    })
    let result = await dynamodb.scan(params).promise();
    result = result.Items.map((item, index) => ({
      ...item,
      photos: result.Items[index].photos.filter(photo => photo.isValidate === true)
    }))
    return result
  }

  /**
  * @function getPublicUser
  * @description récupére toute les infomartion d'un utilisateur par
  * rapport à son id seulement si l'utilisateur est public
  * @param {string} id - Id de l'utilisateur ( partition key )
  */
  async getPublicUser(id) {
     const params = this._createParamObject({
       KeyConditionExpression: 'id = :id',
       FilterExpression: 'isPublic = :public',
       ExpressionAttributeValues: {
         ':id': id,
         ':public': true
       },
       ProjectionExpression: 'height, hairColor, eyesColor, extraType, clothes, id, createdAt, firstname, photos, regionName'
     })
   let result = await dynamodb.query(params).promise()
   result = {
     ...result.Items[0],
     photos: result.Items[0].photos.filter(photo => photo.isValidate === true)
   }
   return result
 }

  _createParamObject(additionalArgs = {}) {
    return {
      ...this._baseParams,
      ...additionalArgs
    }
  }
}

exports.UsersRepository = UsersRepository;
