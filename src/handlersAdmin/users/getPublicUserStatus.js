const { UsersRepository } = require('../../repositoriesAdmin/users.repository.js');
const { response } = require('../../utils/response.util');

const usersRepository = new UsersRepository();

/**
* @function getPublicUserStatus
* @description récupére la liste des utilisateurs public ou non
*/

module.exports.getPublicUserStatus = async (event) => {
	const isPublic = JSON.parse(event.queryStringParameters.isPublic)
	try {
		let result = await usersRepository.getPublicUserStatus(isPublic)
		return response(200, result)
	} catch (error) {
		return response(error.statusCode || 501, {message: error.message})
	}
}
