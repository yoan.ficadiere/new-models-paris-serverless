// var jwt = require('jsonwebtoken');
const { UsersRepository } = require('../../repositoriesAdmin/users.repository.js');
const { response } = require('../../utils/response.util');

const usersRepository = new UsersRepository();

/**
* @function getUser
* @description Permet de récupérer toute les informations
* d'un utilisateur
*/

module.exports.getUser = async (event) => {
  // console.log(event.headers.Authorization);
  // var decodedJwt = jwt.decode(event.headers.Authorization, {complete: true});
  // console.log(decodedJwt);
  try {
    let user = await usersRepository.getUser(event.pathParameters.id);
    if (!user) {
      return response(200, {message: `User width id " ${event.pathParameters.id} " not found`})
    }
    return response(200, user)
  } catch (error) {
    return response(error.statusCode || 501, { message: error.message })
  }
}
