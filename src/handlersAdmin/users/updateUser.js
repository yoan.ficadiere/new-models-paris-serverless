const { UsersRepository } = require('../../repositoriesAdmin/users.repository.js');
const { response } = require('../../utils/response.util');

const usersRepository = new UsersRepository();

/**
* @function updateUser
* @description Permet de mettre à jour les informations sur un utilisateur
*/

module.exports.updateUser = async (event) => {
  const { id } = event.pathParameters
  const { createdAt, informations } = JSON.parse(event.body)
  try {
    let updatedUser = await usersRepository.updateUser(id, createdAt, informations);
    return response(200, updatedUser)
  } catch (error) {
    return response(error.statusCode || 501, { message: error.message })
  }
}
