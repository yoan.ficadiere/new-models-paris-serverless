const { UsersRepository } = require('../../repositoriesAdmin/users.repository.js');
const { response } = require('../../utils/response.util');

const usersRepository = new UsersRepository();

/**
* @function updatePhotos
* @description Permet de mettre à jour les photos d'un utilisateur
*/
module.exports.updatePhotos = async (event) => {
  const { id } = event.pathParameters;
  const { photos, createdAt } = JSON.parse(event.body)
  // Vérifie que au moin une photo est valide
  if (photos.every(photo => photo.isValidate === false)) {
    return response(200, { message: 'you must always have at least one photos validate'})
  }
  try {
    let updated = await usersRepository.updatePhotos(id, createdAt, photos);
    return response(200, { success: true, photos : updated.photos })
  } catch (error) {
    return response(error.statusCode || 501, { message: `${error.message} : error when reordering photos`})
  }
}
