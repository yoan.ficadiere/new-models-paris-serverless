const { UsersRepository } = require('../../repositoriesAdmin/users.repository.js');
const { response } = require('../../utils/response.util');

const usersRepository = new UsersRepository();

/**
* @function getReviewedUsersStatus
* @description récupére la liste des utilisateur qui n'on pas encore
* étais examiné
*/

module.exports.getReviewedUsersStatus = async (event) => {
	const isReviewed = JSON.parse(event.queryStringParameters.isReviewed)
	try {
		let result = await usersRepository.getReviewedUsersStatus(isReviewed)
		return response(200, result)
	} catch (error) {
		return response(error.statusCode || 501, {message: error.message})
	}
}
