const { UsersRepository } = require('../../repositoriesAdmin/users.repository.js');
const { response } = require('../../utils/response.util');
const { sender } = require('../../utils/email/sender.util');

const usersRepository = new UsersRepository();

/**
* @function reviewUser
* @description Permet de valider ou non un utilisateur
*/

module.exports.reviewUser = async (event) => {
  const { id } = event.pathParameters
  const { createdAt, isValidate, emailInformations, reason } = JSON.parse(event.body)
  try {
    let review = await usersRepository.reviewUser(id, createdAt, isValidate);
    if (isValidate === true) {
      await sender('validateUser', emailInformations)
    }
    if (isValidate === false && reason !== 'custom' ) {
      await sender(reason, emailInformations)
    }
    return response(200, review)
  } catch (error) {
    return response(error.statusCode || 501, { message: error.message })
  }
}
