const { UsersRepository } = require('../../repositoriesAdmin/users.repository.js');
const { response } = require('../../utils/response.util');

const usersRepository = new UsersRepository();

/**
* @function getRejectUsers
* @description récupére la liste des utilisateurs rejeté
*/

module.exports.getRejectUsers = async () => {
	try {
		let result = await usersRepository.getRejectUsers()
		return response(200, result)
	} catch (error) {
		return response(error.statusCode || 501, {message: error.message})
	}
}
