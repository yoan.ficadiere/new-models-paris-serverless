const { UsersRepository } = require('../../repositoriesAdmin/users.repository.js');
const { response } = require('../../utils/response.util');

const usersRepository = new UsersRepository();

/**
* @function publicUser
* @description Permet de rendre public un utilisateur
*/

module.exports.publicUser = async (event) => {
  const { id } = event.pathParameters;
  const { isPublic, createdAt } = JSON.parse(event.body)
  try {
    let publicState = await usersRepository.publicUser(id, createdAt, isPublic);
    return response(200, publicState)
  } catch (error) {
    return response(error.statusCode || 501, { message: `${error.message} : please check if your user exist and if it is validate`})
  }
}
