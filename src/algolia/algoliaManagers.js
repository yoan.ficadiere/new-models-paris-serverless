'use strict';

const algoliasearch = require('algoliasearch');
const client = algoliasearch(process.env.ALGOLIA_APP_ID, process.env.ALGOLIA_ADMIN_API_KEY);
const algoliaIndex = process.env.STAGE === 'dev' ? 'dev_new_models_paris' : 'prod_new_models_paris';
const index = client.initIndex(algoliaIndex);

index.setSettings({
  attributesForFaceting: [
    'birthdateTimestamp',
    'clothes',
    'extraType',
    'eyesColor',
    'hairColor',
    'height',
    'sex',
    'regionName',
    'origin'
  ]
});

/**
* @function addModel
* @params {stream} - streamModel flux de donnée
* @description Ajoute les utilisateurs dans algolia pour permettre la recherche
*/
module.exports.addModel = streamModel => {
  const model = prepareModelForAlgolia(streamModel);
  return index.addObject(model);
};

/**
* @function deleteModel
* @params {string} - id du model
* @description Supprime un models de algolia par rapport à son id
*/
module.exports.deleteModel = id => {
  return index.deleteObject(id);
};

/**
* @function prepareModelForAlgolia
* @params {stream} - streamModel flux de donnée
* @description Ajoute uniquement les propriétées voulu dans algolia
*/
let prepareModelForAlgolia = streamModel => {
  console.log('************ stremaModel *************');
  console.log(streamModel);
  console.log('************ stremaModel *************');
  return {
    id: streamModel.id,
    objectID: streamModel.id,
    firstname: streamModel.firstname,
    sex: streamModel.sex,
    birthdate: streamModel.birthdate,
    birthdateTimestamp: streamModel.birthdateTimestamp,
    city: streamModel.city,
    postCode: streamModel.postCode,
    regionName: streamModel.regionName,
    origin: streamModel.origin,
    height: streamModel.height,
    hairColor: streamModel.hairColor,
    eyesColor: streamModel.eyesColor,
    photos: streamModel.photos,
    extraType: streamModel.extraType,
    clothes: streamModel.clothes,
    createdAt: streamModel.createdAt
  };
}
