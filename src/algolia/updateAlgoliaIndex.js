'use strict';

const algoliaManager = require('./algoliaManagers');
const AWS = require('aws-sdk');
// Transformation en format json normale
const parse = AWS.DynamoDB.Converter.output;

/**
* @function updateAlgoliaIndex
* @params {object} - event
* @description Ajoute les utilisateurs dans algolia uniquement
* lors de leur validation, si le status de l'utilisateurs passe à nom
* valide alors ce dernier est supprimer de algolia
*/
module.exports.updateAlgoliaIndex = (event, context, callback) => {
  console.log('updateAlgoliaIndex was called');
  const eventData = event.Records[0];
  const models = parse({ M: eventData.dynamodb.NewImage })
  console.log(models);
  if (eventData.eventName !== 'REMOVE') {
    if (models.isReviewed === true && (models.isValidate === false || models.isPublic === false)) {
      console.log('inside delete');
      algoliaManager.deleteModel(models.id).then((res) => {
        console.log(res);
        callback(null, null);
      }, err => console.log(err));
    } else if (models.isReviewed === true && models.isValidate === true) {
      algoliaManager.addModel(models).then((res) => {
        console.log(res);
        callback(null, null);
      }, err => console.log(err));
    } else {
      console.log(`User with id : ${models.id}, is not validate, if you want add user to algolia please validate it`);
    }
  } else {
    console.log('You have delete user in database');
  }
};
